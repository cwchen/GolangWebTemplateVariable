# [Golang][Web] Variable in http/template

A tiny demo app for variable in [http/template](https://golang.org/pkg/html/template/).

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/GolangWebTemplateVariable.git
```

Run this program:

```
$ cd GolangWebTemplateVariable
$ go run main.go
```

Visit http://localhost:8080 for the result:

![Using Variable in http/template](images/golang-web-template-variable.PNG)

## Copyright

2018, Michael Chen; Apache 2.0.
